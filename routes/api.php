<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api','cors']], function () {
    Route::post('auth/register', 'AuthController@register');
    Route::post('auth/login', 'AuthController@authenticate');
    Route::get('open', 'DataController@open');

    Route::group(['middleware' => ['jwt-auth']], function() {
        Route::get('user', 'AuthController@getAuthenticatedUser');
        Route::get('closed', 'DataController@closed');
        Route::post('auth/logout', 'AuthController@logout');
        Route::get('photo', 'PhotoController@index');
        Route::post('photo', 'PhotoController@store');
        Route::get('photo/{id}', 'PhotoController@show');
        Route::post('photo/{id}', 'PhotoController@update');
        Route::delete('photo/{id}', 'PhotoController@destroy');
        Route::get('album/list', 'PhotoController@listAlbum');
        Route::get('album', 'PhotoController@album');
        Route::post('photo/{id}/album', 'PhotoController@updateAlbum');
    });
});