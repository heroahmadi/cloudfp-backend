<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Photo extends Eloquent
{
	protected $collection = 'photos';
    protected $fillable = [
        'user_id', 'photo', 'album', 'created_at', 'updated_at'
    ];
}
