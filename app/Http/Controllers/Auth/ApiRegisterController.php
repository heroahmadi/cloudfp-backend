<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\User;

class ApiRegisterController extends Controller
{
    /**
     * Handle a registration request for the application.
     *
     * @override
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $errors = $this->validator($request->all())->errors();

        // if(count($errors))
        // {
        //     return response(['errors' => $errors], 401);
        // }

        // event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->save();

        return response(['user' => $user]);
    }
}
