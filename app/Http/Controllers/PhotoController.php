<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Photo;
use Carbon\Carbon;
use App\User;

class PhotoController extends Controller
{
    /**
     * Display all of my photos
     *
     * @return \Illuminate\Http\Response json
     */
    public function index(Request $request)
    {
        $photos = Photo::where('user_id', $request->user_id)->get();

        return response()->json($photos);
    }

    public function listAlbum(Request $request)
    {
        $photos = Photo::where('user_id', $request->user_id)->groupBy('album')->get();

        return response()->json($photos);
    }

    public function album(Request $request)
    {
        $photos = Photo::where('user_id', $request->user_id)
                        ->where('album', $request->album)
                        ->get();
        return response()->json($photos);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Upload and store photo
     *
     * @param  \Illuminate\Http\Request  $request
     *      
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::findOrFail($request->input('user_id'));
        $data = [
            'user_id' => $request->input('user_id'),
            'photo' => $request->input('photo'),
            'album' => $request->input('album'),
            'created_at' => Carbon::now(),
            'updated_at' => null
        ];

        $result = Photo::create($data);

        return response()->json($result->_id);
    }

    /**
     * Display photo by id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($photo_id, Request $request)
    {
        $photo = Photo::findOrFail($photo_id);
        if($request->user_id != $photo->user_id)
            return response()->json(['status' => 403]);
        else
            return response()->json($photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($photo_id, Request $request)
    {
        $photo = Photo::findOrFail($photo_id);
        if($request->user_id != $photo->user_id)
            return response()->json(['status' => 403]);
        else
        {
            $photo->photo = $request->input('photo');
            $photo->save();
            
            return response()->json($photo);
        }
    }

    public function updateAlbum($photo_id, Request $request)
    {
        $photo = Photo::findOrFail($photo_id);
        if($request->user_id != $photo->user_id)
            return response()->json(['status' => 403]);
        else
        {
            $photo->album = $request->input('album');
            $photo->save();
            
            return response()->json($photo);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($photo_id, Request $request)
    {
        $photo = Photo::findOrFail($photo_id);
        if($request->user_id != $photo->user_id)
            return response()->json(['status' => 403]);
        else
        {
            $photo->destroy($photo_id);
            
            return response()->json(['status' => 200]);
        }
    }
}
